import pandas as pd
import numpy as np

frame = pd.DataFrame()
list_data = []


def start(separate=False):
    # get files from 1999 to 2014 and apply transformations
    for year in range(1999, 2015):
        
        # get the desired columns from the csv
        df = pd.read_csv("unprocessed_data/mort{0}.csv".format(year), usecols=requiredCols(year))

        # convert education systems
        df = convertEduc(df, year)

        #make sure they only have 14 columns
        assert df.shape[1] == 12

        # handle null values
        df = handleNulls(df)

        df = handleCompat(df)

        # print year
        print(year)

        if(separate):
            df.to_csv("data/mortality_{0}.csv".format(year))   
        
        list_data.append(df)
    
    # once the loop as ended, merge and create csv
    frame = pd.concat(list_data)
    frame.to_csv("mortality_all.csv")

def requiredCols(year):
    """ given a year, returns the labels of the columns to be used
        for the dataset
     """
    if(year < 2003):
        return ['educ', 'monthdth', 'sex', 'ager12', 'placdth', 'marstat',
        'weekday', 'year', 'injwork', 'mandeath','ucr39', 'race']
    if(year < 2006):
        return ['educ','educ89', 'monthdth', 'sex', 'ager12', 'placdth', 'marstat',
        'weekday', 'year', 'injwork', 'mandeath', 'ucr39', 'race']
    else:
        return ['educ1989','educ2003', 'monthdth', 'sex', 'ager12', 'placdth', 'marstat',
        'weekday', 'year', 'injwork', 'mandeath', 'ucr39', 'race'] 

def convertEduc(df, year):
    """Reduces both education revision columns to just one with the following encoding
            > 0 : No formal education
            > 1 : <=8 years elementary school
            > 2 : 9<= x <= 12 years of highschool
            > 3 : 1 < x < 2 years of college
            > 4 : > 2 years of college
            > 5 : Unknown
        """
    if(year < 2003):
        df.educ.replace(
            [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,99],
            [0,1,1,1,1,1,1,1,1,2,2,2,2,3,3,4,4,4,5], inplace=True)
    elif(year < 2006):
        df.educ89.replace(
            [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,99],
            [0,1,1,1,1,1,1,1,1,2,2,2,2,3,3,4,4,4,5], inplace=True
        )
        df.educ.replace(
            [1,2,3,4,5,6,7,8,9],
            [1,2,2,3,3,4,4,4,5], inplace=True
        )

         # merge both to a new series and remove the old ones
        df["educ"] = df.educ.combine(df.educ89, lambda x,y: y if pd.isnull(x) else x)
        del df["educ89"]
    else:
        df.educ1989.replace(
            [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,99],
            [0,1,1,1,1,1,1,1,1,2,2,2,2,3,3,4,4,4,5], inplace=True
        )
        df.educ2003.replace(
            [1,2,3,4,5,6,7,8,9],
            [1,2,2,3,3,4,4,4,5], inplace=True
        )
        
        # merge both to a new series and remove the old ones
        df["educ"] = df.educ1989.combine(df.educ2003, lambda x,y: y if pd.isnull(x) else x)
        del df["educ1989"]
        del df["educ2003"] 

    return df


def handleCompat(df):
    """
    Replace some old values with new encoding
    """
    df.rename(columns={'race':'ethnicity'})

    df.sex.replace([1,2],['M','F'], inplace=True)
    df.marstat.replace([1,2,3,4,8,9],['S','M','W','D','U','U'],inplace=True)

    return df


def handleNulls(df):
    """
        Replace all the null values with the proper unknown value
    """
    # education replace with 5 - Unknown
    df.educ.fillna(5, inplace=True) 

    # ager12 replace with 12 - Not Stated
    df.ager12.fillna(12, inplace=True)
    
    # place of death replace with 9 - Unkown
    df.placdth.fillna(9, inplace=True)
    
    # marital status replace with U - unkown
    df.marstat.fillna('U', inplace=True)
    
    # day of the week replace with 9 - unknown
    df.weekday.fillna(9, inplace=True)
    
    # injury at work replace with U - unknown
    df.injwork.fillna("U", inplace=True)

    # maner of death replace with 9 - not specified
    df.mandeath.fillna(9, inplace=True)

    assert df.isnull().values.any()==False

    return df

# when the program is run, call method start()
if __name__ == '__main__':
	start(True)