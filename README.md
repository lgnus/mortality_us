Description of the files available in this repository:

* mortality_all.zip : Dataset used in the tableau project; 
* raw_data_process.py : Python script used to create the dataset from the original datasets from CDC pertaining to mortality in the US.
* VID.twb : Tableau file containing the project developed
* VID.twbx : Tableau packaged workbook containing the project 
* Report.pdf : Final report of the project.

Authors: 
* José Carneiro nº41749 
* Jorsivania da Silva nº50830 